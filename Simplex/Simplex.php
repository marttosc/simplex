<?php
namespace Simplex;

class Simplex
{
    /**
     * Número máximo de voltas.
     * @var integer
     */
    public $maxSteps;

    /**
     * Número de steps.
     * @var integer
     */
    public $steps = 0;

    /**
     * Variáveis básicas.
     * @var array
     */
    public $varBasicas;

    /**
     * Solução do simplex.
     * @var array
     */
    public $solucao;

    /**
     * Função Z.
     * @var double
     */
    public $z;

    /**
     * Construtor do Simplex.
     * @param integer $maxStop
     */
    public function __construct($maxStop)
    {
        $this->maxSteps = $maxStop;
    }

    /**
     * Resolve o método Simplex.
     * @param  Padrao $padrao
     * @return void
     */
    public function resolver(Padrao $padrao)
    {
        $this->varBasicas = array();

        for ($j = 1; $j < $padrao->n - 1; $j++) {
            if ($padrao->tableau[0][$j] != 0) {
                $this->varBasicas[$j - 1] = false;
            } else {
                $this->varBasicas[$j - 1] = false;

                $num1 = 0;
                $num0 = 1;

                for ($i = 1; $i < $padrao->m; $i++) {
                    if ($padrao->tableau[$i][$j] == 0) {
                        $num0++;
                    } else if ($padrao->tableau[$i][$j] == 1) {
                        $num1++;
                    }
                }

                if ($num1 == 1 && $num0 == $padrao->m - 1) {
                    $this->varBasicas[$j - 1] = true;
                }
            }
        }

        while ($this->resolvido($padrao) == false && $this->steps <= $this->maxSteps) {
            $this->steps++;

            $colunaEntrada = 1;

            if ($padrao->max) {
                for ($i = 0; $i < count($this->varBasicas); $i++) {
                    if (!$this->varBasicas[$i] && $padrao->tableau[0][$i + 1] < $padrao->tableau[0][$colunaEntrada]) {
                        $colunaEntrada = $i + 1;
                    }
                }
            } else {
                for ($i = 0; $i < count($this->varBasicas); $i++) {
                    if (!$this->varBasicas[$i] && $padrao->tableau[0][$i + 1] > $padrao->tableau[0][$colunaEntrada]) {
                        $colunaEntrada = $i + 1;
                    }
                }
            }

            $minTaxR = -1;
            $minTax = -1;

            for ($i = 1; $i < $padrao->m; $i++) {
                $denom = $padrao->tableau[$i][$colunaEntrada];

                if ($denom > 0) {
                    $minTaxR = $i;
                    $minTax = $padrao->tableau[$i][$padrao->n - 1] / $denom;
                }

                if ($minTaxR != -1) {
                    break;
                }
            }

            for ($i = $minTaxR + 1; $i < $padrao->m; $i++) {
                $denom = $padrao->tableau[$i][$colunaEntrada];

                if ($denom > 0) {
                    $tax = $padrao->tableau[$i][$padrao->n - 1] / $denom;

                    if ($tax < $minTax) {
                        $minTaxR = $i;
                        $minTax = $tax;
                    }
                }
            }

            $denom = $padrao->tableau[$minTaxR][$colunaEntrada];

            for ($j = 0; $j < $padrao->n; $j++) {
                $padrao->tableau[$minTaxR][$j] = $padrao->tableau[$minTaxR][$j] / $denom;

                if ($padrao->tableau[$minTaxR][$j] == -0) {
                    $padrao->tableau[$minTaxR][$j] = 0;
                }
            }

            for ($i = 0; $i < $padrao->m; $i++) {
                if ($i != $minTaxR) {
                    $c = -$padrao->tableau[$i][$colunaEntrada];

                    for ($j = 0; $j < $padrao->n; $j++) {
                        $padrao->tableau[$i][$j] = $padrao->tableau[$i][$j] + $c * $padrao->tableau[$minTaxR][$j];
                    }
                }
            }

            for ($j = 1; $j < $padrao->n - 1; $j++) {
                if ($padrao->tableau[0][$j] != 0) {
                    $this->varBasicas[$j - 1] = false;
                } else {
                    $this->varBasicas[$j - 1] = false;

                    $num1 = 0;
                    $num0 = 1;

                    for ($i = 1; $i < $padrao->m; $i++) {
                        if ($padrao->tableau[$i][$j] == 0) {
                            $num0++;
                        } else if ($padrao->tableau[$i][$j] == 1) {
                            $num1++;
                        }
                    }

                    if ($num1 == 1 && $num0 == $padrao->m - 1) {
                        $this->varBasicas[$j - 1] = true;
                    }
                }
            }
        }
    }

    /**
     * Verifica se a solução foi resolvida.
     * @param  Padrao   $padrao
     * @return boolean
     */
    public function resolvido(Padrao $padrao)
    {
        $resolvido = false;

        if ($padrao->max) {
            $negVar = 0;

            for ($i = 0; $i < count($this->varBasicas); $i++) {
                if (!$this->varBasicas[$i] && $padrao->tableau[0][$i + 1] < 0) {
                    $negVar++;
                }
            }

            if ($negVar == 0) {
                $resolvido = true;

                $this->solucao = array();

                for ($j = 0; $j < count($this->varBasicas); $j++) {
                    if ($this->varBasicas[$j]) {
                        for ($i = 1; $i < $padrao->m; $i++) {
                            if ($padrao->tableau[$i][$j + 1] == 1) {
                                $this->solucao[$j] = $padrao->tableau[$i][$padrao->n - 1];
                            }
                        }
                    }
                }

                $this->z = $padrao->tableau[0][$padrao->n - 1];
            }
        } else {
            $posVar = 0;

            for ($i = 0; $i < count($this->varBasicas); $i++) {
                if (!$this->varBasicas[$i] && $padrao->tableau[0][$i + 1] > 0) {
                    $posVar++;
                }
            }

            if ($posVar == 0) {
                $resolvido = true;

                $this->solution = array();

                for ($j = 0; $j < count($this->varBasicas); $j++) {
                    if ($this->varBasicas[$j]) {
                        for ($i = 1; $i < $padrao->m; $i++) {
                            if ($padrao->tableau[$i][$j + 1] == 1) {
                                $this->solucao[$j] = $padrao->tableau[$i][$padrao->n - 1];
                            }
                        }
                    }
                }

                $z = $padrao->tableau[0][$padrao->n - 1];
            }
        }

        return $resolvido;
    }
}