<?php
namespace Simplex;

class Restricao
{
    /**
     * Elementos da restrição.
     * @var array
     */
    public $elementos;

    /**
     * Sinal da restrição.
     * @var integer
     */
    public $sinal;

    const MENOR_IGUAL = 0;
    const MAIOR_IGUAL = 1;
    const IGUAL = 2;

    /**
     * Construtor de uma Restrição.
     * @param array     $elementos
     * @param integer   $sinal
     */
    public function __construct(array $elementos, $sinal)
    {
        $this->elementos = $elementos;
        $this->sinal     = $sinal;
    }
}