<?php
namespace Simplex;

class Padrao
{
    /**
     * Equações, ou linhas.
     * @var integer
     */
    public $m;

    /**
     * Variáveis, ou colunas.
     * @var integer
     */
    public $n;

    /**
     * Variáveis "artificiais".
     * @var integer
     */
    public $a = 0;

    /**
     * Maior linha.
     * @var integer
     */
    public $maiorM = 0;

    /**
     * É para maximizar?
     * @var boolean
     */
    public $max;

    /**
     * Tableau do método.
     * @var array
     */
    public $tableau;

    /**
     * Matriz do $tableau.
     * @var Chippyash\Matrix\Matrix
     */
    public $Tableau;

    /**
     * Construtor de uma solução padrão.
     * @param array     $z
     * @param boolean   $max
     * @param array     $restricoes
     */
    public function __construct(array $z, $max, array $restricoes)
    {
        $this->max = $max;

        $this->m = count($restricoes) + 1;
        $this->n = 2 * (count($restricoes[0]->elementos) - 1) - 2;

        for ($i = 0; $i < count($restricoes); $i++) {
            if ($restricoes[$i]->elementos[count($restricoes[$i]->elementos) - 1] < 0) {
                for ($j = 0; $j < count($restricoes[$i]->elementos); $j++) {
                    $restricoes[$i]->elementos[$j] = - $restricoes[$i]->elementos[$i];

                    if ($restricoes[$i]->sinal == Restricao::MENOR_IGUAL) { $restricoes[$i]->sinal = Restricao::MAIOR_IGUAL; }
                    if ($restricoes[$i]->sinal == Restricao::MAIOR_IGUAL) { $restricoes[$i]->sinal = Restricao::MENOR_IGUAL; }
                }
            }
        }

        for ($i = 0; $i < count($restricoes); $i++) {
            if ($restricoes[$i]->sinal > Restricao::MENOR_IGUAL) $this->a++;
        }

        $this->n += $this->a;

        $this->tableau = [];

        $this->tableau[0][0] = 1;

        for ($j = 0; $j < count($z); $j++) {
            $this->tableau[0][$j + 1] = -$z[$j];
        }

        if ($this->max) {
            $menorZ = $z[0];

            for ($j = 1; $j < count($z); $j++) {
                if ($z[$j] < $menorZ) $menorZ = $z[$j];
            }

            $this->maiorM = 1000 * $menorZ;

            for ($j = $this->n - $this->a; $j < $this->n; $j++) {
                $this->tableau[0][$j - 1] = $this->maiorM;
            }
        } else {
            $maiorZ = $z[0];

            for ($j = 1; $j < count($z); $j++) {
                if ($z[$j] > $maiorZ) $maiorZ = $z[$j];
            }

            $this->maiorM = 1000 * $maiorZ;

            for ($j = $this->n - $this->a; $j < $this->n; $j++) {
                $this->tableau[0][$j - 1] = -$this->maiorM;
            }
        }

        $artificial = 0;

        for ($i = 1; $i < $this->m; $i++) {
            for ($j = 0; $j < count($restricoes[0]->elementos) - 1; $j++) {
                $this->tableau[$i][$j + 1] = $restricoes[$i - 1]->elementos[$j];
            }

            if ($restricoes[$i - 1]->sinal == Restricao::MENOR_IGUAL) {
                $this->tableau[$i][count($restricoes[0]->elementos) + $i - 1] = 1;
            } else if ($restricoes[$i - 1]->sinal == Restricao::MAIOR_IGUAL) {
                $this->tableau[$i][count($restricoes[0]->elementos) + $i -1] = -1;

                $this->tableau[$i][$this->n - $this->a - 1 + $artificial] = 1;

                $artificial++;
            } else if ($restricoes[$i - 1]->sinal == Restricao::IGUAL) {
                $this->tableau[$i][$this->n -$this->a - 1 + $artificial] = 1;

                $artificial++;
            }

            $this->tableau[$i][$this->n - 1] = $restricoes[$i - 1]->elementos[count($restricoes[$i - 1]->elementos) - 1];
        }

        if ($this->a > 0) {
            for ($j = 1; $j < $this->n; $j++) {
                for ($i = 1; $i < $this->m; $i++) {
                    if ($restricoes[$i - 1]->sinal > Restricao::MENOR_IGUAL) {
                        if ($this->max) {
                            $this->tableau[0][$j] += -$this->maiorM * $this->tableau[$i][$j];
                        } else {
                            $this->tableau[0][$j] += $this->maiorM * $this->tableau[$i][$j];
                        }
                    }
                }
            }
        }

        $this->Tableau = new \Chippyash\Matrix\Matrix($this->tableau);
    }
}