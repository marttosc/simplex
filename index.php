<?php
require_once 'vendor/autoload.php';

require_once 'Simplex/Restricao.php';
require_once 'Simplex/Padrao.php';
require_once 'Simplex/Simplex.php';

use Simplex\Restricao;
use Simplex\Padrao;
use Simplex\Simplex;

$z = [3, 5];
// $z = [2, 3];
$max = true;
// $max = false;
$rest = [
    new Restricao([1, 4], Restricao::MENOR_IGUAL),
    new Restricao([2, 6], Restricao::MENOR_IGUAL),
    new Restricao([3, 2, 18], Restricao::MENOR_IGUAL),
    // new Restricao([0.5, 0.25, 4], Restricao::MENOR_IGUAL),
    // new Restricao([1, 3, 20], Restricao::MAIOR_IGUAL),
    // new Restricao([1, 1, 10], Restricao::IGUAL),
];

$padrao = new Padrao($z, $max, $rest);

$simplex = new Simplex(20);

$simplex->resolver($padrao);

for ($i = 0; $i < count($simplex->solucao); $i++) {
    echo ($i + 1) . ': ' .$simplex->solucao[$i] . '<br>';
}

echo 'z: ' . $simplex->z . '<br>';
echo 'steps: ' . $simplex->steps . '<br>';